import { shallowMount, mount } from "@vue/test-utils";
import UploadFile from "@/components/UploadFile.vue";

describe("UploadFile", () => {
  it("renders correctly", () => {
    const wrapper = mount(UploadFile, {
      propsData: {
        files: [],
        max: 100,
        intervals: {},
        intervalScan: {},
        isBackground: false,
        listFolder: [],
      },
    });

    expect(wrapper.exists()).toBe(true);
    expect(wrapper).toBeDefined();
    expect(wrapper).not.toBeNull();
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("render button upload", () => {
    const wrapper = mount(UploadFile);

    expect(wrapper.findAll("b-button")).toHaveLength(2);
  });

  it("renders Upload file button and hidden file input", () => {
    const wrapper = mount(UploadFile);
    const fileBtn = wrapper.find(
      'b-button[variant="outline-primary"][id="btnInputFile"]'
    );
    const fileInput = wrapper.find(
      'input[type="file"][name="file"][hidden="hidden"]'
    );

    expect(fileBtn.exists()).toBe(true);
    expect(fileBtn.text()).toBe("Upload file");
    expect(fileInput.exists()).toBe(true);
    expect(fileInput.attributes("multiple")).toBe("multiple");
  });

  it("renders Upload folder button and hidden folder input", () => {
    const wrapper = mount(UploadFile);
    const folderBtn = wrapper.find(
      'b-button[variant="outline-primary"][id="btnInputFolder"]'
    );
    const folderInput = wrapper.find(
      'input[type="file"][name="file"][hidden="hidden"][webkitdirectory]'
    );

    expect(folderBtn.exists()).toBe(true);
    expect(folderBtn.text()).toBe("Upload folder");
    expect(folderInput.exists()).toBe(true);
    expect(folderInput.attributes("multiple")).toBe("multiple");
  });

  it("test call function handleChangeFile when click button", () => {
    const wrapper = shallowMount(UploadFile, {
      propsData: {
        files: [],
        max: 100,
        intervals: {},
        intervalScan: {},
        isBackground: false,
        listFolder: [],
      },
    });
    const handleClickSpy = jest.spyOn(wrapper.vm, "handleOpen");
    const findBtn = wrapper.findAll('b-button[variant="outline-primary"]');

    findBtn.trigger("click");

    expect(handleClickSpy).toHaveBeenCalled();
    expect(handleClickSpy).toHaveBeenCalledTimes(2);

    handleClickSpy.mockRestore();
  });

  it("handles file input change", async () => {
    const wrapper = shallowMount(UploadFile);
    const mockFile = new File(["content"], "mockFileName");
    const mockEvent = { target: { files: [mockFile] } };

    const spyHandleChangeListFile = jest.spyOn(wrapper.vm, "addFiles");

    await wrapper.vm.handleChangeFile(mockEvent);

    expect(spyHandleChangeListFile).toHaveBeenCalledWith([mockFile], "file");
    spyHandleChangeListFile.mockRestore();
  });

  it("handles folder input change", async () => {
    const wrapper = shallowMount(UploadFile);
    const mockFile = new File(["content"], "exampleFolder", { type: "" });
    const mockEvent = { target: { files: [mockFile] } };

    const spyHandleChangeListFolder = jest.spyOn(wrapper.vm, "addFiles");

    await wrapper.vm.handleChangeFolder(mockEvent);

    expect(spyHandleChangeListFolder).toHaveBeenCalledWith(
      [mockFile],
      "folder"
    );
    spyHandleChangeListFolder.mockRestore()
  });

  it("handles file and folder drop", async () => {
    const wrapper = shallowMount(UploadFile);
    const spyHandleChangeListFile = jest.spyOn(
      wrapper.vm,
      "addFiles"
    );
    const spyReadDirectory = jest.spyOn(wrapper.vm, "readDirectory");

    const mockEvent = {
      preventDefault: jest.fn(),
      dataTransfer: {
        items: [
          {
            kind: "file",
            webkitGetAsEntry: jest.fn(() => ({ isDirectory: false })),
          },
          {
            kind: "file",
            webkitGetAsEntry: jest.fn(() => ({ isDirectory: true })),
          },
        ],
      },
    };

    await wrapper.vm.handleDrop(mockEvent);

    expect(mockEvent.preventDefault).toHaveBeenCalled();
    expect(spyHandleChangeListFile).toHaveBeenCalled();
    expect(spyReadDirectory).toHaveBeenCalled();
  });
});
